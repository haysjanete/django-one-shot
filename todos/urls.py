from django.urls import path, include
from todos.views import TodoListListView, TodoListDetailView

urlpatterns = [
    path("<int:pk>/", TodoListDetailView.as_view(), name="todo_list_detail"),
    path("", TodoListListView.as_view(), name="todo_list_list"),
]
