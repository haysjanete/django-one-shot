# Generated by Django 4.0.6 on 2022-07-26 17:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('todos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='TodoItem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('task', models.CharField(max_length=100)),
                ('due_date', models.DateTimeField()),
                ('is_complete', models.BooleanField(verbose_name=False)),
                ('list', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Items', to='todos.todolist')),
            ],
        ),
    ]
