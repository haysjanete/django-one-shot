from datetime import datetime
from enum import auto
from tkinter import CASCADE
from xmlrpc.client import DateTime
from django.db import models
from django.forms import DateTimeField


class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField()
    is_complete = models.BooleanField(False)
    list = models.ForeignKey(
        TodoList,
        related_name="Items",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.task
