from todos.models import TodoList
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic import CreateView

# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    template_name = "list.html"
    context_object_name = "todolist_name"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "detail.html"
    context_object_name = "todolistdetail"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        print(context)
        return context


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "create.html"
